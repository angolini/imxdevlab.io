This report was generated on Wed Mar 21 20:07:25 UTC 2018

 |**PDF file** |**Last modification**|
|[IMX6ULLRM.pdf](http://www.nxp.com/docs/en/reference-manual/IMX6ULLRM.pdf)                 | Mon Nov 13 00:12:28 2017                  |
|[IMX6SXRM.pdf](http://www.nxp.com/docs/en/reference-manual/IMX6SXRM.pdf)                 | Thu Sep 7 09:00:16 2017                  |
|[IMX6ULRM.pdf](http://www.nxp.com/docs/en/reference-manual/IMX6ULRM.pdf)                 | Wed Apr 13 16:29:19 2016                  |
|[iMX6DQPRM.pdf](http://www.nxp.com/docs/en/reference-manual/iMX6DQPRM.pdf)                 | Thu Aug 31 13:45:42 2017                  |
|[IMX6DQRM.pdf](http://www.nxp.com/docs/en/reference-manual/IMX6DQRM.pdf)                 | Tue Sep 12 15:36:01 2017                  |
|[IMX6SLRM.pdf](http://www.nxp.com/docs/en/reference-manual/IMX6SLRM.pdf)                 | Thu Aug 31 20:19:45 2017                  |
|[IMX6SDLRM.pdf](http://www.nxp.com/docs/en/reference-manual/IMX6SDLRM.pdf)                 | Thu Aug 31 20:15:50 2017                  |
