This report was generated on Wed Mar 21 20:12:34 UTC 2018

 |**PDF file** |**Last modification**|
|[IMX6ULLCE.pdf](http://www.nxp.com/docs/en/errata/IMX6ULLCE.pdf)                 | Wed Oct 11 14:26:37 2017                  |
|[IMX6SXCE.pdf](http://www.nxp.com/docs/en/errata/IMX6SXCE.pdf)                 | Thu Apr 7 10:46:50 2016                  |
|[IMX6ULCE.pdf](http://www.nxp.com/docs/en/errata/IMX6ULCE.pdf)                 | Wed Oct 11 13:28:22 2017                  |
|[IMX6DQCE.pdf](http://www.nxp.com/docs/en/errata/IMX6DQCE.pdf)                 | Thu Aug 18 09:58:37 2016                  |
|[IMX6SLCE.pdf](http://www.nxp.com/docs/en/errata/IMX6SLCE.pdf)                 | Wed Jul 16 13:18:27 2014                  |
|[IMX6SDLCE.pdf](http://www.nxp.com/docs/en/errata/IMX6SDLCE.pdf)                 | Wed Aug 17 10:28:29 2016                  |
