---
title: MikroE NFC Click Board on i.MX7D
date: 2017-05-24 14:23:31
author: marco
categories: [ Tutorial ]
tags: [ MikroE, IMX7D, PN7120, NFC, Click_boards, L4.1.15_2.0.0_GA]
---
{% include base_path %}

This tutorial guides on setting up and running the **NFC Demo App** using the [PN7120 NFC Click board](https://shop.mikroe.com/click/wireless-connectivity/nfc) with the i.MX 7Dual SABRE-SD and its mikroBUS socket compatible.

<center><img src="{{ base_path }}/images/NFC_click_board.jpg" alt="i.MX 7Dual with NFC Click Board" align="middle" width="600"/></center>
<center><figcaption align="middle"> i.MX 7Dual with NFC Click Board </figcaption></center>

## NXP BSP Release

To set up and build an image for the i.MX 7Dual SABRE-SD, the ```RootFS``` used was ```fsl-image-gui``` with frame buffer (fb) backend. The instructions can be found in the ```Yocto User’s Guide``` included on the ```L4.1.15_2.0.0-ga BSP``` Release Doc Bundle. Other images may be used but its needs to include the ```libstdc++.so.6``` library required by the NFC Demo App.
It needs to build and install the toolchain for cross compiling the kernel. This  [post](https://imxdev.gitlab.io/tutorial/How_to_build_Yocto_SDK_and_compile_for_i.MX/) shows how to compile the SDK.

## Linux Kernel

With an image running and the SDK compiled, the next step is modifying the kernel. In the host Linux PC, create a folder and download the repository:
```console
$ mkdir imx7dsabresd
$ cd imx7dsabresd
$ git clone http://git.freescale.com/git/cgit.cgi/imx/linux-2.6-imx.git
$ git checkout imx_4.1.15_2.0.0_ga
```
Still in the imx7dsabresd kernel repository, open the misc driver folder and clone the ```pn5xx``` driver too:
```console
$ cd ~/imx7dsabresd/build_dir/drivers/misc
$ git clone https://github.com/NXPNFCLinux/nxp-pn5xx.git
```
Apply these two patches for the dts file and to configure the ```pn5xx``` driver:

### NFC dts file

Download the patch from <a href="{{base_path}}/patches/ARM_dts_imx7d-sdb_Add_NFC_Click_Board_support.patch"> here</a>

```diff
From b8bd419f0f87a0f942202a5d1b7d3588916c9d9d Mon Sep 17 00:00:00 2001
From: Marco Franchi <marco.franchi@nxp.com>
Date: Tue, 23 May 2017 12:49:33 -0300
Subject: [PATCH] ARM: dts: imx7d-sdb: Add NFC Click Board support

The imx7d-sdb has a mikro bus socket that can be connected to a
NFC click board (PN7120 near field communications controller).

https://shop.mikroe.com/click/wireless-connectivity/nfc

Add a new device tree file to describe such hardware.

Signed-off-by: Marco Franchi <marco.franchi@nxp.com>
---
 arch/arm/boot/dts/Makefile          |  1 +
 arch/arm/boot/dts/imx7d-sdb-nfc.dts | 24 ++++++++++++++++++++++++
 2 files changed, 25 insertions(+)
 create mode 100644 arch/arm/boot/dts/imx7d-sdb-nfc.dts

diff --git a/arch/arm/boot/dts/Makefile b/arch/arm/boot/dts/Makefile
index c107c29..1717939 100644
--- a/arch/arm/boot/dts/Makefile
+++ b/arch/arm/boot/dts/Makefile
@@ -420,6 +420,7 @@ dtb-$(CONFIG_SOC_IMX7D) += \
 	imx7d-sdb-m4.dtb \
 	imx7d-sdb-qspi.dtb \
 	imx7d-sdb-mipi-dsi.dtb \
+	imx7d-sdb-nfc.dtb \
 	imx7d-sdb-reva.dtb \
 	imx7d-sdb-reva-epdc.dtb \
 	imx7d-sdb-reva-gpmi-weim.dtb \
diff --git a/arch/arm/boot/dts/imx7d-sdb-nfc.dts b/arch/arm/boot/dts/imx7d-sdb-nfc.dts
new file mode 100644
index 0000000..055d9ff
--- /dev/null
+++ b/arch/arm/boot/dts/imx7d-sdb-nfc.dts
@@ -0,0 +1,24 @@
+/*
+ * Copyright (C) 2015 Freescale Semiconductor, Inc.
+ *
+ * This program is free software; you can redistribute it and/or modify
+ * it under the terms of the GNU General Public License version 2 as
+ * published by the Free Software Foundation.
+ */
+
+#include "imx7d-sdb.dts"
+
+&i2c3 {
+	clock-frequency = <100000>;
+	pinctrl-names = "default";
+	pinctrl-0 = <&pinctrl_i2c3>;
+	status = "okay";
+
+	pn547: pn547@28 {  
+		compatible = "nxp,pn547";  
+		reg = <0x28>;  
+		clock-frequency = <400000>;  
+		interrupt-gpios = <&gpio6 14 1>;  
+		enable-gpios = <&gpio_spi 3 1>;  
+    };
+};
--
2.7.4
```
### Include pn5xx to the kernel compilation

Download the patch from <a href="{{base_path}}/patches/misc_pn5xx_Include_the_driver_to_the_kernel.patch"> here</a>

```diff
From df11641da3d7f88c5d6bc5ae41b7cf16358a778e Mon Sep 17 00:00:00 2001
From: Marco Franchi <marco.franchi@nxp.com>
Date: Fri, 26 May 2017 10:03:10 -0300
Subject: [PATCH] misc: (pn5xx) Include the driver to the kernel

Include the pn5xx driver and the driver config to the kernel
compilation.

Signed-off-by: Marco Franchi <marco.franchi@nxp.com>
---
 drivers/misc/Kconfig  | 1 +
 drivers/misc/Makefile | 1 +
 2 files changed, 2 insertions(+)

diff --git a/drivers/misc/Kconfig b/drivers/misc/Kconfig
index fa77f81..97eac873 100644
--- a/drivers/misc/Kconfig
+++ b/drivers/misc/Kconfig
@@ -548,4 +548,5 @@ source "drivers/misc/mic/Kconfig"
 source "drivers/misc/genwqe/Kconfig"
 source "drivers/misc/echo/Kconfig"
 source "drivers/misc/cxl/Kconfig"
+source "drivers/misc/nxp-pn5xx/Kconfig"
 endmenu
diff --git a/drivers/misc/Makefile b/drivers/misc/Makefile
index 89a33288..64d0a21 100644
--- a/drivers/misc/Makefile
+++ b/drivers/misc/Makefile
@@ -54,6 +54,7 @@ obj-$(CONFIG_VMWARE_VMCI)	+= vmw_vmci/
 obj-$(CONFIG_LATTICE_ECP3_CONFIG)	+= lattice-ecp3-config.o
 obj-$(CONFIG_SRAM)		+= sram.o
 obj-y				+= mic/
+obj-y 				+= nxp-pn5xx/
 obj-$(CONFIG_GENWQE)		+= genwqe/
 obj-$(CONFIG_ECHO)		+= echo/
 obj-$(CONFIG_VEXPRESS_SYSCFG)	+= vexpress-syscfg.o
--
2.7.4
```

Go back to the imx7dsabresd kernel repository and export the environment variables:
```console
$ cd ~/imx7dsabresd/build_dir/
$ source /opt/fsl-imx-fb/4.1.15-2.0.0/environment-setup-cortexa7hf-neon-poky-linux-gnueabi
$ export ARCH=arm
$ export CROSS_COMPILE=$TARGET_PREFIX
$ make imx_v7_defconfig
$ make menuconfig
```
Inside ```menuconfig``` include the driver as a module ```(<M>)```, save the changes and exit:

<center><img src="{{ base_path }}/images/pn5xx_menuconfig.png" alt="NFC_NXP_PN5XX Menuconfig" align="middle" width="800"/></center>
<center><figcaption align="middle"> NFC_NXP_PN5XX Menuconfig </figcaption></center>

Then compile the kernel to include all these modifications:
```console
$ make -j4
```
Copy the ```zImage``` and the dtb files to the SD Card:
```console
$ sudo cp arch/arm/boot/zImage /media/USER/Boot \imx7d/
$ sudo cp arch/arm/boot/imx7d-sdb.dtb imx7d-sdb-nfc.dtb /media/USER/Boot \imx7d/
```
Now, compile and copy the modules to the SD card too:
```console
$ mkdir temp_modules
$ make modules
$ make modules_install INSTALL_MOD_PATH=/temp_modules
$ sudo cp * /temp_modules media/USER/ROOTFS_PATH/lib/
```

## NFC library

This section shows how to install the libraries needed to run the NFC Demo App.
Be sure to install the tool below before continue:
```console
$ sudo apt-get install automake
$ sudo apt-get install autoconf
$ sudo apt-get install libtool
```
Go to the imx7dsabresd directory and clone the ```libnfc-nci```:
```console
$ cd ~/imx7sabresd
$ git clone https://github.com/NXPNFCLinux/linux_libnfc-nci.git
```
Generate the configuration script by executing the ```bootstrap``` bash script:
```console
$ cd /linux_libnfc-nci
$ ./bootstrap
```
To the ```Makefile```, as the NFC Click board uses the **PN7120** and using the default toolchain sysroots path, enter with the command line below:
```console
$ mkdir temp_config
$ ./configure --enable-pn7120 --host=arm-none-linux --prefix=/opt/fsl-imx-fb/4.1.15-2.0.0/sysroots/   x86_64-pokysdk-linux/usr --sysconfdir=/temp_config
$ make
$ make install
```
Copy the libraries directory to the target:
```console
$ cd temp_config
$ sudo cp * /media/USER/ROOTFS_PATH/etc
$ cd ~/imx7sabresd/.libs/
$ sudo cp * /media/USER/ROOTFS_PATH/usr/lib
$ sudo cp nfcDemoApp /media/USER/ROOTFS_PATH/usr/sbin
$ cd ~/imx7sabresd/linux_libnfc-nci/conf/PN7120
$ sudo cp * /media/USER/ROOTFS_PATH/etc
```

## Testing the NFC Demo App

Start the board and change de U-Boot ```fdt_file``` variable:
```console
=> setenv fdt_file imx7s-sdb-nfc.dtb
=> saveenv
=> reset
```
Run the commands below from the Linux prompt on the i.MX 7Dual SABRE-SD board:
```console
$ modprobe pn5xx_i2c
$ nfcDemoApp poll
```
<iframe width="560" height="315" src="https://www.youtube.com/embed/qNBg9iN0LGA" frameborder="0" allowfullscreen></iframe>

I plan to create a Yocto Project image to make the installation process easier. I will update here when this becomes available.
This post was based on the [AN11697 Application note](http://www.nxp.com/documents/application_note/AN11697.pdf) and the  [PN7150/PN7120 NFC Controller SBC Kit for Arduino on the i.MX6SX](https://community.nxp.com/docs/DOC-331906) tutorial.
