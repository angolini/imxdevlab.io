---
title: H.264 GStreamer pipelines examples for non-VPU SoCs - Part 1 playback
date: 2017-08-09 16:31:07
author: marco
categories: [ Tutorials ]
tags: [ GStreamer, H264, IMX7D, IMX7S, IMX6UL, IMX6ULL, IMX6SL, IMX6SLL, IMX6SX, x264enc, avdec_h264, encoder, decoder, L4.1.15_2.0.0_GA ]
---
This post shows some GStreamer pipelines examples for ramping you up on using H.264 on non-VPU SoCs.

{% include base_path %}
{% include toc icon="th-list" title="Examples" %}

All these GStreamer pipelines were tested in the kernel BSP release 4.1.15-2.0.0 GA using i.MX 7Dual SABRE-SD and i.MX 6UltraLite EVK. These pipelines can be also used on other non-VPU SoCs.
For installing H.264 plugins in a non-VPU board, please follow this [post](https://imxdev.gitlab.io/tutorial/Encoding_and_decoding_H264_without_VPU).

## Video Playback
The video used in these tests was [big_bucky_bunny_480p_h264.mov](http://download.blender.org/peach/bigbuckbunny_movies/big_buck_bunny_480p_h264.mov).
```
gst-launch-1.0 filesrc location=<video_file> ! decodebin ! autovideosink
```

`Decodebin` and `autovideosink` choose the decoder plugin and the videosink to be used based on the priority of each plugin installed.

On non-VPU boards, as the process is executed only by the CPU, which can easily reach 100% and cause high system latency, you can face the error `There may be a timestamping problem, or this computer is too slow`. Some `queues` can be added with `max-size-time` property and the `sync=false` property to the `autovideosink` to avoid this error. The `queue` adds some control over the buffer, thereby limiting the frame delay. The `sync=false` drops the frames which are unlikely to be decoded on time. This option prioritizes the video flow, but might miss some frames:
```
gst-launch-1.0 filesrc location=<video_file> ! decodebin ! queue max-size-time=0 ! autovideosink sync=false
```

## Camera Playback
```
gst-launch-1.0 v4l2src device=/dev/video1 ! video/x-raw,width=640,height=480 ! autovideosink
```
Follow this [post](https://imxdev.gitlab.io/tutorial/How_to_use_OV5640_with_iMX_6UltraLite_EVK) to set up the i.MX 6UltraLite EVK camera.
{: .notice--info}

## H.264 Software Video Decoder
```
gst-launch-1.0 filesrc location=<video_file> ! qtdemux ! queue max-size-time=0 ! avdec_h264 ! imxv4l2sink sync=false
```
This pipeline has the same function as the **Video Playback** example, but a GStreamer pipeline can be easily modified by the user according to different needs.

## H.264 Software Video + Audio Decoder
This example adds audio decoder to the **H.264 Software Video Decoder** example:
```
gst-launch-1.0 filesrc location=<video_file> ! qtdemux name=d d. ! queue ! avdec_h264 ! autovideosink sync=false d. ! queue ! beepdec ! alsasink sync=false
```

## H.264 Software Video Encoder
```
gst-launch-1.0 videotestsrc num-buffers=50 ! 'video/x-raw,format=(string)NV12,width=1280,height=720,framerate=(fraction)30/1' ! x264enc ! mp4mux ! filesink location=video-h264-1280x720.mp4
```
This example gets the first 50 frames at HD resolution and saves them as a `.mp4` file using the H.264 encoder plugin `x264enc` and `mp4mux`.

## H.264 Software Camera Encoder
```
gst-launch-1.0 imxv4l2src device=/dev/video1 num-buffers=50 ! videoconvert ! 'video/x-raw,format=(string)NV12,width=640,height=480,framerate=(fraction)30/1' ! queue ! x264enc ! mp4mux ! filesink sync=false location=camera-h264-640x480.mp4
```
This GStreamer pipeline gets the first 50 frames at VGA resolution and saves them as a `.mp4` file using the H.264 encoder plugin `x264enc`. This is different than the **H.264 Software Video Encoder** example because `x264enc` does not support `YUY2` colorspace format. This pipeline needs the `videoconvert` to change `YUY2` to `NV12`.
