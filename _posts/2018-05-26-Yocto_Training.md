---
title: Yocto Training
date: 2018-05-26 13:51:13
author: daiane
categories: [Tutorial]
tags: [yocto]
author_profile: false
classes:
  - wide
sidebar:
  - title: "Tasks"
    text: "
The very first steps to begin working with the Yocto Project for IMX:

1. [Downloading the source code](/tutorial/Yocto_Training/#task1)

2. [First build](/tutorial/Yocto_Training/#task2)

3. [The build result](/tutorial/Yocto_Training/#task3)

4. [Deploy and test](/tutorial/Yocto_Training/#task4)

5. [Linux Kernel](/tutorial/Yocto_Training/#task5)

6. [Customize the image](/tutorial/Yocto_Training/#task6)

7. [Create a toolchain](/tutorial/Yocto_Training/#task7)

8. [Build kernel manually using the created toolchain](/tutorial/Yocto_Training/#task8)

9. [How to add bad/ugly](/tutorial/Yocto_Training/#task9)

10. [How to create a custom layer](/tutorial/Yocto_Training/#task10)
"
  - title: "FSL Community BSP"
    text: "
* [Release Notes](http://freescale.github.io/doc/release-notes/current/index.html)

* [Pre Built Images](http://freescale.github.io/#downloads)
"
  - title: "Does Yocto support?"
    text: "
* [Machines](http://layers.openembedded.org/layerindex/branch/master/machines/)

* [Recipes](http://layers.openembedded.org/layerindex/branch/master/recipes/)
"

---

{% include base_path %}

This tutorial was first published at [here](https://community.nxp.com/docs/DOC-94849)
and I would never imagine it would be so relevant and useful until today (the
first version is from 2013 - and I still receive messages asking to an update)

The first version was written for _dora_ branch. Although much of the old content
is still up to date. I prefer to properly review the content.

Please, feel free to let a comment with your questions.

<h2 id="{{ "task1" | slugify }}" class="archive__subtitle">Task 1 - Downloading the source code</h2>

The focus of this tutorial is to introduce the Yocto Project to be used with IMX.
The project support several other architectures and machines. Search if your
machine is supported [here](http://layers.openembedded.org/layerindex/branch/master/machines/)

### Step by step

{% highlight shell linenos%}
// download repo
$ mkdir ~/bin
$ curl http://commondatastorage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
$ chmod a+x ~/bin/repo
$ PATH=${PATH}:~/bin 
// download the metadata for IMX support
$ mkdir fsl-community-bsp
$ cd fsl-community-bsp
$ repo init -u https://github.com/Freescale/fsl-community-bsp-platform -b rocko
$ repo sync
{% endhighlight %}

At the end of `repo sync` you have:

{% highlight shell linenos%}
user@pc:~/fsl-community-bsp$ tree -L 2
.
├── README -> sources/base/README
├── setup-environment -> sources/base/setup-environment
└── sources
    ├── base
    ├── Documentation
    ├── meta-freescale
    ├── meta-freescale-3rdparty
    ├── meta-freescale-distro
    ├── meta-openembedded
    └── poky

8 directories, 2 files
{% endhighlight %}

### The alternative

Alternatively, you can download some pre-built images available
[here](http://freescale.github.io/#download)

### Understand

In order to install the Yocto Project source code (the set of metadata: recipes
and configuration files) you need to clone several git repository.

The FSL Community BSP prepared an environment you can use `repo` utility to
download all those at once (the `repo init` + `repo sync` lines)

The following commands are the equivalent, but without using `repo`.

```console
$ mkdir -p ~/fsl-community-bsp/sources
$ cd ~/fsl-community-bsp/sources
$ git clone https://git.yoctoproject.org/git/poky -b rocko
$ git clone https://git.yoctoproject.org/git/meta-freescale -b rocko
$ git clone https://github.com/Freescale/meta-freescale-distro -b rocko
$ git clone https://github.com/Freescale/meta-freescale-3rdparty -b rocko
```

You can see the description of each meta layer in the FSL Community BSP
[release notes](http://freescale.github.io/doc/release-notes/2.4/index.html#document-bsp-scope)

<h2 id="{{ "task2" | slugify }}" class="archive__subtitle">Task 2 - First Build</h2>
<h2 id="{{ "task2" | slugify }}" class="archive__subtitle">Task 3 - The build result</h2>
<h2 id="{{ "task2" | slugify }}" class="archive__subtitle">Task 4 - Deploy and test</h2>
<h2 id="{{ "task2" | slugify }}" class="archive__subtitle">Task 5 - Linux Kernel</h2>
<h2 id="{{ "task2" | slugify }}" class="archive__subtitle">Task 6 - Customize the image</h2>
<h2 id="{{ "task2" | slugify }}" class="archive__subtitle">Task 7 - Create a toolchain</h2>
<h2 id="{{ "task2" | slugify }}" class="archive__subtitle">Task 8 - Build kernel manually using the created toolchain</h2>
<h2 id="{{ "task2" | slugify }}" class="archive__subtitle">Task 9 - How to add bad/ugly</h2>
<h2 id="{{ "task2" | slugify }}" class="archive__subtitle">Task 10 - How to create a custom layer</h2>

