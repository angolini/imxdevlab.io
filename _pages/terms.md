---
layout: single
title: Terms and Privacy Policy
permalink: /terms/
---

## Cookies

This blog uses cookies for Google Analytics and for Disqus. 
<BR>
This is an open source blog, if you have any doubt, please see the source code in the <a href="http://gitlab/{{ site.author.gitlab }}"><u>GitLab</u></a> source code.
<BR>
If you have questions, please open an <a href="http://gitlab/{{ site.author.gitlab }}/issues"><u>Issue</u></a>.

## {{ site.title }}

The {{ site.title }} content (the posts under _posts/ and the pages) is under <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons 4.0 ShareAlike </a> and the authorship of each post can be seen on top of each post and also on git history at <a href="http://gitlab/{{ site.author.gitlab }}"><u>GitLab</u></a>.
